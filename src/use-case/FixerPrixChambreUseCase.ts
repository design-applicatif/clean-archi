import { ReceptionPort } from "../hexagone/ReceptionGateway";

export class FixerPrixChambreUseCase {
  constructor(private readonly receptionPort: ReceptionPort) {}

  execute(nouveauPrix: number) {
    var listeChambres = this.receptionPort.recupererChambres();
    for (const chambre of listeChambres) {
        
      chambre.modifierPrix(nouveauPrix);
    }
    return this.receptionPort.mettreAJourChambres(listeChambres);
  }
}
