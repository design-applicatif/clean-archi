import { Chambre } from "../hexagone/Chambre";
import { ReceptionPort } from "../hexagone/ReceptionGateway";

interface PresenterRecupererChambres {

    execute(listeDeChambre: Chambre[]): void 
}

export class PresenterString implements PresenterRecupererChambres {
    constructor(private chambres: Chambre[] = []) {}

    execute(listeDeChambre: Chambre[]): void {
        this.chambres = listeDeChambre
    }

}

export class RecupererChambresUseCase {
  constructor(private readonly receptionPort: ReceptionPort) {}

  execute(presenter: PresenterRecupererChambres) {
    const chambres = this.receptionPort.recupererChambres();
    presenter.execute(chambres);

  }
}
