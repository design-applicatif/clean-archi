import { describe } from "node:test";
import { Chambre } from "./Chambre";
import { ReceptionTestAdapter } from "./ReceptionTestAdapter";
import { FixerPrixChambreUseCase } from "../use-case/FixerPrixChambreUseCase";

describe("Fixer le prix des chambres du rez de chaussée", () => {
  it("doit enregistrer le prix modifié des chambres du rez de chaussée", () => {
    // given
    const useCase = new FixerPrixChambreUseCase(
      new ReceptionTestAdapter([new Chambre(0, 1, 50)])
    );
    // when
    useCase.execute(70);
    // then
    expect(useCase.recupererChambres()).toEqual([new Chambre(0, 1, 70)]);
  });
  it("doit enregistrer le prix modifié des chambres du 1er étage d'une valeur du prix du rez de chaussée + 7%", () => {
    // given
    const useCase = new FixerPrixChambreUseCase(
      new ReceptionTestAdapter([new Chambre(1, 1, 50)])
    );
    // when
    useCase.execute(100);
    // then
    expect(useCase.recupererChambres()).toEqual([new Chambre(1, 1, 107)]);
  });
  it("doit enregistrer le prix modifié des chambres du 2eme étage d'une valeur du prix du rez de chaussée + 22%", () => {
    // given
    const useCase = new FixerPrixChambreUseCase(
      new ReceptionTestAdapter([new Chambre(2, 1, 50)])
    );
    // when
    useCase.execute(100);
    // then
    expect(useCase.recupererChambres()).toEqual([new Chambre(2, 1, 122)]);
  });
  it("doit enregistrer le prix modifié des chambres du 3eme étage d'une valeur du prix du rez de chaussée + 33%", () => {
    // given
    const useCase = new FixerPrixChambreUseCase(
      new ReceptionTestAdapter([new Chambre(3, 1, 50)])
    );
    // when
    useCase.execute(100);
    // then
    expect(useCase.recupererChambres()).toEqual([new Chambre(3, 1, 133)]);
  });
  describe("lorsque le montant du prix final calculé dépasse les 200€", () => {
    it("pour le rez de chaussée le prix sera de 200€ maximum", () => {
      // given
      const useCase = new FixerPrixChambreUseCase(
        new ReceptionTestAdapter([new Chambre(0, 1, 50)])
      );
      // when
      useCase.execute(1000);
      // then
      expect(useCase.recupererChambres()).toEqual([new Chambre(0, 1, 200)]);
    });
    it("pour les étages supérieurs le prix sera de 200€ maximum", () => {
      // given
      const useCase = new FixerPrixChambreUseCase(
        new ReceptionTestAdapter([new Chambre(3, 1, 50)])
      );
      // when
      useCase.execute(190);
      // then
      expect(useCase.recupererChambres()).toEqual([new Chambre(3, 1, 200)]);
    });
  });
});
