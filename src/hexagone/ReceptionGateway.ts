import { Chambre } from "./Chambre";

export interface ReceptionPort {
  recupererChambres(): Chambre[];
  mettreAJourChambres(chambres: Chambre[]): void;
}
